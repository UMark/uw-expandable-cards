# Expandable Cards

Turns an unordered list into columns of expandable cards.

## Version 1.0.14

## NPM package

### Installation

You must add an .npmrc file to your project or your home directory to tell NPM where the @UMark registry is located. Add the following line to your .npmrc file:
```
@UMark:registry=https://git.doit.wisc.edu/api/v4/packages/npm/
```
Once you've done that, you can install the NPM package either by running this on your command line:
```
npm i @UMark/uw-expandable-cards --save
```
Or by adding this to your dependencies in package.json:
```
"@UMark/uw-expandable-cards": "^1.0.0"
```

### Use

See examples and documentation in project's examples folder.

## For Developers

### Installation

1. Clone project to your local machine.

2. Install npm packages
```
npm install
```
3. Build scripts and compile styles
```
npm run build-dev
```

### Running project

4. Start your favorite server to view examples and documentation from your browser. Examples are in the examples folder. See package.json for additional scripts for development.


### Publish npm package

Be sure to build for production before publishing.
```
npm run build-prod
```
```
npm publish
```

## Changelog

### [1.0.14] 2022-11-14

#### Added

- Option to pass searchIndex array to be used by fuse instead of default innerText of cards

### [1.0.13] 2022-5-31

#### Fixed

- Sets aria-hidden=true on collapsed cards.

### [1.0.12] 2022-3-29

#### Added

- Adjusted animation.

### [1.0.11] 2022-3-28

#### Added

- Added option to enable scrollTo opened card.

### [1.0.10] 2022-2-14

#### Changed

- Moved search data set up into separate function so that it can be overwritten.

### [1.0.9] 2022-2-9

#### Changed

- Removed aria-live="polite" region to update number of results. Left in option to display result count.
- Added option for aria-live="assertive" region to alert screen reader users when search returns no results

#### Removed

- Removed functionality to create elements for search input and reset buttons. Users must now create those elements in the document and pass the selectors to the filter constructor.

### [1.0.8] 2022-2-8

#### Added

- Added support for details and button elements in the card. The will be removed from and added to the tab order as needed.

#### Removed

- Removed unneeded references to the close button, since the close button was removed some time ago.

### [1.0.7] 2022-1-21

#### Changed

- Removed period from end of filter message indicating how many items are shown
- Updated code that generates button so that any markup in the item heading is copied to the button

### [1.0.6] 2022-1-21

#### Fixed

- Removed logging from dist in npm package

### [1.0.5] 2021-11-18

#### Fixed

- Fixed miscalculation when resizing in list view

### [1.0.4] 2021-11-17

#### Added

- Added support for IOS 7

#### Changed

- Hid list view toggle button when screen width less than 600px (37.5em) and cards are displayed in a single column. Can be overwritten in a stylesheet.
- Tweaked CSS so card content is not indented when cards are displayed in a singe column.
