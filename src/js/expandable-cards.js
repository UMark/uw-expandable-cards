import debounce from 'lodash.debounce'
/* global Object, define */

(function (factory) {
  typeof define === 'function' && define.amd
    ? define(factory)
    : typeof exports === 'object'
      ? (module.exports = factory())
      : factory()
})(function () {
  ('use strict')
  const _window = typeof window !== 'undefined' ? window : this
  const UWExpandableCards = (_window.UWExpandableCards = function (itemsSelector, settings) {
    const _ = this
    _.opt = Object.assign(
      {},
      {
        columns: 4,
        headingEl: 'h3',
        listView: false,
        imgClone: true,
        scrollTo: false
      },
      settings
    )
    if (_.opt.scrollTo) {
      _.opt.isSmoothScrollSupported = 'scrollBehavior' in document.documentElement.style
    }

    if (typeof itemsSelector === 'string') {
      _.itemsElement = document.querySelector(itemsSelector)
    } else if (typeof itemsSelector === 'object') {
      _.itemsElement = itemsSelector
    }

    // Adds correct class for number of columns -- four columns is the default
    _.itemsElement.classList.add('uw-expandable-cards')
    if (_.opt.columns === 3) {
      _.itemsElement.classList.add('uwec-three-col')
    } else if (_.opt.columns === 2) {
      _.itemsElement.classList.add('uwec-two-col')
    } else if (_.opt.columns === 1) {
      _.itemsElement.classList.add('uwec-one-col')
    }

    _.items = _.itemsElement.children

    // Add event listeners
    _.init()
  })

  UWExpandableCards.prototype.init = function () {
    const _ = this
    _.listView = false
    const idBase = Math.floor(Math.random() * 1000)
    const triggers = []
    // Create a paragraph element for screen readers explaining that this is an accordion element
    const srp = document.createElement('p')
    srp.textContent = 'This is an accordion element with a series of buttons that open and close related content panels.'
    srp.classList.add('uwec-show-for-sr')
    _.itemsElement.parentNode.insertBefore(srp, _.itemsElement)
    // Wraps item content in a div, adds toggle and close Btns
    for (let i = 0; i < _.items.length; i++) {
      // Create the toggle Btn to open and close the card
      const headingEl = _.items[i].querySelector(_.opt.headingEl)
      const id = 'uwec-' + idBase + i
      const btnId = 'uwecbtn-' + idBase + i
      headingEl.setAttribute('aria-hidden', true)
      const title = headingEl.innerHTML
      const toggleBtnHeading = document.createElement(_.opt.headingEl)
      const toggleBtn = document.createElement('button')
      toggleBtn.innerHTML = title
      toggleBtn.classList.add('uwec-toggle-btn')
      toggleBtn.setAttribute('aria-expanded', 'false')
      toggleBtn.setAttribute('aria-controls', id)
      toggleBtn.setAttribute('id', btnId)
      toggleBtnHeading.appendChild(toggleBtn)
      triggers.push(toggleBtn)
      let btnDiv = toggleBtnHeading

      // Images included
      if (_.opt.imgSelector) {
        // create a div to put the h3 and button and image inside
        btnDiv = document.createElement('div')
        btnDiv.appendChild(toggleBtnHeading)
        const img = _.items[i].querySelector(_.opt.imgSelector)
        if (img) {
          const imgWrapper = document.createElement('div')
          if (_.opt.imgClone) {
            imgWrapper.appendChild(img.cloneNode())
          } else {
            imgWrapper.appendChild(img)
          }
          btnDiv.appendChild(imgWrapper)
          _.items[i].appendChild(btnDiv)
        }
      }
      btnDiv.classList.add('uwec-btn-wrapper')

      // Create div to wrap the content in
      const div = document.createElement('div')
      div.setAttribute('id', id)
      div.classList.add('uwec-content')
      div.setAttribute('aria-hidden', 'true')

      while (_.items[i].childNodes.length > 0) {
        div.appendChild(_.items[i].childNodes[0])
      }
      // Add div to the DOM
      _.items[i].appendChild(btnDiv)
      _.items[i].appendChild(div)
      _.items[i].classList.add('uwec-item')
      // Hide links inside the descriptions
      _.hideDescrLinks(_.items[i])
    }

    // Resize event listener
    const resizeDebounce = debounce(_.onResize.bind(_), 300)
    window.addEventListener('resize', resizeDebounce)

    // Toggle between list and grid view
    if (_.opt.listView) {
      // Add list view Btn to DOM before wrapper and add event listener for it
      _.toggleViewBtn = document.createElement('button')
      _.toggleViewBtn.classList.add('uwec-toggle-view')
      _.toggleViewBtn.textContent = 'View as list'
      _.toggleViewBtn.setAttribute('aria-label', 'Toggle view')
      _.toggleViewBtn.setAttribute('aria-pressed', 'false')
      _.itemsElement.parentNode.insertBefore(_.toggleViewBtn, _.itemsElement)
      _.toggleViewBtn.addEventListener('click', _.toggleView.bind(_))
    }

    // Keydown event listener for items wrapper -- up/down arrow nav through cards
    _.itemsElement.addEventListener('keydown', function (e) {
      if (e.target.closest('button') === null) {
        // do nothing -- clicking on something in the descr
      } else {
        // what key was pressed?
        const key = e.which.toString()
        if (key.match(/38|40/)) {
          const index = triggers.indexOf(e.target)
          const direction = (key.match(/40/)) ? 1 : -1
          const length = triggers.length
          const newIndex = (index + length + direction) % length
          triggers[newIndex].focus()
          e.preventDefault()
        }
      }
    })
    // Click event listener for items wrapper -- toggles the
    // cards open and closed
    _.itemsElement.addEventListener('click', function (e) {
      if (e.target.closest('.uwec-btn-wrapper') === null) {
        // do nothing -- clicking on something in the descr
      } else {
        _.toggleCard(e)
      }
    })

    _.w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
    _.wrapperPos = _.getPosition(_.itemsElement)
    _.itemsElement.classList.add('uwec-initialized')
  }

  UWExpandableCards.prototype.toggleView = function () {
    const _ = this
    // any cards open? if so click closed
    _.itemsElement.classList.add('uwec-fade')
    const openCard = _.itemsElement.querySelector('.uwec-open')
    if (openCard) {
      _.closeCard()
    }

    setTimeout(function () {
      _.listView = !_.listView
      if (_.listView) {
        _.toggleViewBtn.textContent = 'View as grid'
        _.toggleViewBtn.setAttribute('aria-pressed', 'true')
        _.itemsElement.classList.add('uwec-list-view')
      } else {
        _.itemsElement.classList.remove('uwec-list-view')
        _.toggleViewBtn.textContent = 'View as list'
        _.toggleViewBtn.setAttribute('aria-pressed', 'false')
      }
      _.itemsElement.classList.remove('uwec-fade')
    }, 470)
  }

  UWExpandableCards.prototype.toggleCard = function (e) {
    const _ = this
    _.currentParent = e.target.closest('.uwec-item')
    _.currentBtn = _.currentParent.querySelector('button')
    _.currentPosition = _.getPosition(_.currentParent)
    _.currentDescr = _.currentParent.querySelector('.uwec-content')
    _.currentIndex = _.getKeyByItem()
    _.currentSiblings = _.findSiblings()
    if (_.currentParent.classList.contains('uwec-open')) {
      // if we clicked on the current Btn and the current parent is open, just close the card
      _.currentDescr.classList.remove('uwec-reveal')
      _.hideDescrLinks(_.currentParent)
      _.currentBtn.setAttribute('aria-expanded', 'false')
      _.currentDescr.setAttribute('aria-hidden', 'true')
      setTimeout(function () {
        _.currentParent.classList.remove('uwec-open')
        for (let i = 0; i < _.items.length; i++) {
          _.items[i].style.transform = 'translateY(0)'
          _.items[i].style.webkitTransform = 'translateY(0)'
        }
        _.itemsElement.style.paddingBottom = 0
      }, 200)
    } else {
      // if we clicked the Btn for a card that is not open
      _.currentBtn.setAttribute('aria-expanded', 'true')
      _.currentDescr.setAttribute('aria-hidden', 'false')
      // set positioning for card to open
      _.setLeftPosition()
      // get all of the cards in the right location
      let transformAmt
      if (_.listView) {
        transformAmt = _.currentDescr.offsetHeight
      } else {
        transformAmt = _.currentDescr.offsetHeight + 32
      }
      const transY = 'translateY(' + transformAmt + 'px)'
      _.currentDescr.style.bottom = '-' + transformAmt + 'px'
      _.itemsElement.style.paddingBottom = transformAmt + 'px'

      let currentTransAmt = _.currentParent.style.transform
      if (typeof currentTransAmt === 'undefined') {
        currentTransAmt = 0
      } else {
        currentTransAmt = currentTransAmt.replace(/[^\d.]/g, '')
      }

      for (let i = 0; i < _.items.length; i++) {
        if (i > _.currentIndex && _.currentSiblings.indexOf(_.items[i]) === -1) {
          _.items[i].style.transform = transY
          _.items[i].style.webkitTransform = transY
        } else {
          _.items[i].style.transform = 'translateY(0)'
          _.items[i].style.webkitTransform = 'translateY(0)'
        }
      }
      if (_.prevParent && _.prevParent !== _.currentParent) {
        // close the previous parent if there was one
        _.prevDescr.classList.remove('uwec-reveal')
        _.prevDescr.setAttribute('aria-hidden', 'true')
        _.prevParent.classList.remove('uwec-open')
        _.prevBtn.setAttribute('aria-expanded', 'false')
        _.hideDescrLinks(_.prevParent)
      }
      // show the current card
      setTimeout(function () {
        _.currentParent.classList.add('uwec-open')
        _.currentDescr.classList.add('uwec-reveal')
      }, 300)
      _.showDescrLinks(_.currentParent)
      if (_.opt.scrollTo) {
        const rect = _.currentParent.getBoundingClientRect()
        let top = rect.top
        if (currentTransAmt !== '' && currentTransAmt > 0) {
          top = rect.top - currentTransAmt
        }
        if (_.opt.isSmoothScrollSupported) {
          window.scrollTo({
            top: window.scrollY + top,
            left: 0,
            behavior: 'smooth'
          })
        } else {
          window.scroll(0, (window.scrollY + top))
        }
      }
    }

    // Assign current card to previous card -- wait for animations to complete
    setTimeout(function () {
      _.prevBtn = _.currentBtn
      _.prevParent = _.currentParent
      _.prevDescr = _.currentDescr
      _.prevIndex = _.currentIndex
      _.prevSiblings = _.currentSiblings
    }, 600)
  }

  /*
  * Gets the index of the currently clicked on Btn
  */
  UWExpandableCards.prototype.getKeyByItem = function () {
    const _ = this
    let key
    for (let i = 0; i < _.items.length; i++) {
      if (_.items[i] === _.currentParent) {
        key = i
      }
    }
    return key
  }

  /*
  * Finds the sibling elements of the currently clicked on Btn
  */
  UWExpandableCards.prototype.findSiblings = function () {
    const _ = this
    const siblings = []
    const top = _.currentPosition.top
    let nextParent = _.currentParent.nextElementSibling
    let prevParent = _.currentParent.previousElementSibling
    if (nextParent) {
      let nextPos = _.getPosition(nextParent)
      let nextTop = nextPos.top
      // have to account for the fact that some of the majors may be hidden from view
      while (nextTop === top || nextParent.classList.contains('uwec-hide-item')) {
        if (!nextParent.classList.contains('uwec-hide-item')) {
          siblings.push(nextParent)
        }
        nextParent = nextParent.nextElementSibling
        if (nextParent) {
          nextPos = _.getPosition(nextParent)
          nextTop = nextPos.top
        } else {
          break // last row -- get outta here!
        }
      }
    }
    if (prevParent) {
      let prevPos = _.getPosition(prevParent)
      let prevTop = prevPos.top
      while (prevTop === top || prevParent.classList.contains('uwec-hide-item')) {
        if (!prevParent.classList.contains('uwec-hide-item')) {
          siblings.push(prevParent)
        }
        prevParent = prevParent.previousElementSibling
        if (prevParent) {
          prevPos = _.getPosition(prevParent)
          prevTop = prevPos.top
        } else {
          break // last row -- get outta here!
        }
      }
    }
    return siblings
  }

  /*
  * Gets the position of the passed-in element
  */
  UWExpandableCards.prototype.getPosition = function (el) {
    const rect = el.getBoundingClientRect()
    const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
  }

  UWExpandableCards.prototype.hideDescrLinks = function (item) {
    const links = item.querySelector('.uwec-content').querySelectorAll('a, details, summary, button')
    if (links) {
      for (let j = 0; j < links.length; j++) {
        links[j].setAttribute('tabIndex', '-1')
      }
    }
  }

  UWExpandableCards.prototype.showDescrLinks = function (item) {
    const links = item.querySelector('.uwec-content').querySelectorAll('a, details, summary, button')
    if (links) {
      for (let i = 0; i < links.length; i++) {
        links[i].setAttribute('tabIndex', '0')
      }
    }
  }

  /*
  * Sets the left position of the item description
  */
  UWExpandableCards.prototype.setLeftPosition = function () {
    const _ = this
    if (_.listView) {
      // 16 is to account for the 16px negative margin on wrapper element
      const descLeft = -(_.currentPosition.left - _.wrapperPos.left - 16)
      _.currentDescr.style.left = descLeft + 'px'
    } else {
      // 48 is to account for the 16px negative margin on wrapper element and to give the description element 32px breathing room all around
      const descLeft = -(_.currentPosition.left - _.wrapperPos.left - 48)
      _.currentDescr.style.left = descLeft + 'px'
    }
  }

  UWExpandableCards.prototype.onResize = function () {
    const _ = this
    _.newW = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
    if (_.newW !== _.w) {
      _.wrapperPos = _.getPosition(_.itemsElement)
      if (_.listView) {
        _.wrapperPos.left = _.wrapperPos.left - 16
      }
      _.w = _.newW
      _.closeCard()
    }
  }

  UWExpandableCards.prototype.closeCard = function () {
    const _ = this
    if (_.currentParent) {
      _.currentDescr.classList.remove('uwec-reveal')
      _.currentParent.classList.remove('uwec-open')
      _.currentBtn.setAttribute('aria-expanded', false)
      _.hideDescrLinks(_.currentParent)
      for (let i = 0; i < _.items.length; i++) {
        _.items[i].style.transform = 'translateY(0)'
        _.items[i].style.webkitTransform = 'translateY(0)'
      }
      _.itemsElement.style.paddingBottom = 0
    }
  }

  return UWExpandableCards
})
