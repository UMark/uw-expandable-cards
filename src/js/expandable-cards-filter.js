import debounce from 'lodash.debounce'
import Fuse from 'fuse.js/dist/fuse.basic.min.js'

/* global define */

;(function (factory) {
  typeof define === 'function' && define.amd
    ? define(factory)
    : typeof exports === 'object'
      ? (module.exports = factory())
      : factory()
})(function () {
  ('use strict')
  const _window = typeof window !== 'undefined' ? window : this
  const UWExpandableCardsFilter = (_window.UWExpandableCardsFilter = function (expandableCardsInstance, settings) {
    const _ = this
    _.ecInstance = expandableCardsInstance
    _.items = _.ecInstance.items
    _.itemsElement = _.ecInstance.itemsElement

    _.opt = Object.assign(
      {},
      {},
      settings
    )
    _.filterInputs = []
    _.filterDataSelectors = []
    if (_.opt.filters && Array.isArray(_.opt.filters)) {
      for (let i = 0; i < _.opt.filters.length; i++) {
        _.filterInputs.push(document.querySelector(_.opt.filters[i].filterSelector))
        _.filterDataSelectors.push(_.opt.filters[i].filterDataAttribute)
      }
    }

    _.init()
  })

  UWExpandableCardsFilter.prototype.init = function () {
    const _ = this

    // Using search?
    if (_.opt.searchSelector) {
      _.searchInput = document.querySelector(_.opt.searchSelector)

      if (_.opt.searchAriaLiveSelector) {
        _.ariaLive = document.querySelector(_.opt.searchAriaLiveSelector)
      }
      _.searchDebounce = debounce(_.search.bind(_), 500)
      _.setUpSearch()

      _.searchInput.addEventListener('keyup', _.searchDebounce)
    }

    // Insert reset button
    if (_.opt.resetSelector) {
      _.resetBtn = document.querySelector(_.opt.resetSelector)
      _.resetBtn.addEventListener('click', function (e) {
        for (let i = 0; i < _.filterInputs.length; i++) {
          _.filterInputs[i].selectedIndex = 0
        }
        if (_.opt.searchSelector) {
          _.searchInput.value = ''
        }
        _.ecInstance.closeCard()
        _.showFilteredItems()
      })
    }

    // Insert message container
    if (_.opt.showResultCount) {
      _.messageContainer = document.createElement('p')
      _.messageContainer.textContent = 'All items shown'
      _.itemsElement.parentNode.insertBefore(_.messageContainer, _.itemsElement)
    }

    // Add event listeners
    for (let i = 0; i < _.filterInputs.length; i++) {
      _.filterInputs[i].addEventListener('change', function (e) {
        _.ecInstance.closeCard()
        _.showFilteredItems()
      })
    }
  }

  UWExpandableCardsFilter.prototype.setUpSearch = function () {
    const _ = this
    _.searchData = []
    if (_.opt.searchIndex) {
      _.searchData = _.opt.searchIndex
    }

    for (let i = 0; i < _.ecInstance.items.length; i++) {
      _.searchData.push(_.ecInstance.items[i].innerText.replace(/[^\x20-\x7E]+/g, ''))
    }

    const fuseOptions = {
      minMatchCharLength: 3,
      threshold: 0.0,
      ignoreLocation: true
    }
    setTimeout(function () {
      _.fuse = new Fuse(_.searchData, fuseOptions)
    }, 0)
  }

  UWExpandableCardsFilter.prototype.search = function () {
    const _ = this
    _.ecInstance.closeCard()
    _.showFilteredItems()
  }

  UWExpandableCardsFilter.prototype.getCheckedFilters = function () {
    const _ = this
    const checked = []
    for (var i = 0; i < _.filterInputs.length; i++) {
      checked.push(_.filterInputs[i].value)
    }
    return checked
  }

  UWExpandableCardsFilter.prototype.showFilteredItems = function () {
    const _ = this
    _.totalMatched = 0
    _.checked = _.getCheckedFilters()

    _.itemsElement.classList.add('uwec-fade')
    setTimeout(function () {
      const resultIndexes = _.getSearchMatches()
      for (let i = 0; i < _.items.length; i++) {
        if (_.matchesFilters(_.items[i]) && _.matchesSearch(resultIndexes, i)) {
          _.items[i].classList.remove('uwec-hide-item')
          _.totalMatched++
        } else {
          _.items[i].classList.add('uwec-hide-item')
        }
      }
      _.updateMessage()
    }, 300)

    setTimeout(function () {
      _.itemsElement.classList.remove('uwec-fade')
    }, 600)
  }

  UWExpandableCardsFilter.prototype.getSearchMatches = function () {
    const _ = this
    let resultIndexes = null
    if (_.opt.searchSelector && _.searchInput.value !== '') {
      const results = _.fuse.search(_.searchInput.value)
      resultIndexes = []
      if (results) {
        for (let i = 0; i < results.length; i++) {
          resultIndexes.push(results[i].refIndex)
        }
      }
    }
    return resultIndexes
  }

  UWExpandableCardsFilter.prototype.matchesSearch = function (resultIndexes, itemIndex) {
    let matches = false
    if (resultIndexes === null) {
      matches = true
    } else {
      if (resultIndexes.indexOf(itemIndex) > -1) {
        matches = true
      }
    }
    return matches
  }

  UWExpandableCardsFilter.prototype.matchesFilters = function (item) {
    const _ = this
    var matches = true

    for (var i = 0; i < _.filterDataSelectors.length; i++) {
      if (_.checked[i] === '') {
        matches = matches && true
      } else {
        const values = item.getAttribute(_.filterDataSelectors[i]).split(' ')
        if (values.indexOf(_.checked[i]) === -1) {
          matches = matches && false
        }
      }
    }
    return matches
  }

  UWExpandableCardsFilter.prototype.updateMessage = function () {
    const _ = this
    if (_.opt.showResultCount) {
      if (_.totalMatched === _.items.length) {
        _.messageContainer.textContent = 'All items shown'
      } else {
        _.messageContainer.textContent = _.totalMatched + ' item(s) matched filters'
      }
    }
    if (_.opt.searchAriaLiveSelector) {
      if (_.totalMatched === 0) {
        _.ariaLive.innerText = 'No items found'
      } else {
        _.ariaLive.innerText = ''
      }
    }
  }

  return UWExpandableCardsFilter
})
