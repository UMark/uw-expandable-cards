module.exports = {
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    'standard'
  ],
  rules: {
    'func-call-spacing': 'off',
    'no-func-assign': 'off',
    'no-extend-native': 'off',
    'no-mixed-operators': 'off',
    'wrap-iife': 'off'
  }
}
