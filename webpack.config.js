const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');
const options = {}

module.exports = {
  entry: {
    'expandable-cards': './src/js/expandable-cards.js',
    'expandable-cards-filter': './src/js/expandable-cards-filter.js',
    'expandable-cards-compat': './src/js/expandable-cards-compat.js'
  },
  output: {
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  plugins: [
    new ESLintPlugin(options)
  ]
};
